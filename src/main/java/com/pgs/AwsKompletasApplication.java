package com.pgs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsKompletasApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwsKompletasApplication.class, args);
    }
}
